const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GeneralCommentSchema = new Schema({
    flagged: {
        type: Boolean
    },
    deleted: {
        type: Boolean
    },
    message: {
        type: String
    }
});

const CandidateSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    los: {
        type: String
    },
    written_test: {
        type: Number,
        min: [0, 'Nothing lower than 0'],
        max: [35, "Let's not get carried away"]
    },
    coding_test: {
        type: Number,
        min: [0, 'Nothing lower than 0'],
        max: [35, "Let's not get carried away"]
    },
    technical_interview: {
        type: Number,
        min: [0, 'Nothing lower than 0'],
        max: [5, "Let's not get carried away"]
    },
    management_interview: {
        type: Number,
        min: [0, 'Nothing lower than 0'],
        max: [5, "Let's not get carried away"]
    },
    consultative_interview: {
        type: Number,
        min: [0, 'Nothing lower than 0'],
        max: [5, "Let's not get carried away"]
    },
    image: {
        type: String
    },
    star: {
        type: Number
    },
    veto: {
        type: Number
    },
    general_comment: [GeneralCommentSchema]
});



module.exports = mongoose.model('Candidate', CandidateSchema);