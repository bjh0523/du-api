const express = require('express');
const Login = require('../models/login');

const loginRouter = express.Router();

loginRouter.post('/', (req, res) => {
    let login = new Login();
    login.username = req.body.username;
    login.password = req.body.password;
    Login.find({username: req.body.username}, (_, document) => {
        if(document.length){
            res.status(400).send('User already exists.');
        }
        else {
            login.save((err, document) => {
                if (err) {
                    res.status(400).send(err);
                } else {
                    res.send(`Saved your login!\n${document}`)
                }
            });
        }
    })
});

loginRouter.get('/', (req, res) => {
    Login.find((err, documents) => {
        if (err) {
            res.status(400).send(err);
        } else {
            res.json(documents);
        }
    })
})

module.exports = loginRouter;