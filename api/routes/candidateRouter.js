const express = require('express');
const Candidate = require('../models/candidate');

const candidateRouter = express.Router();

candidateRouter.post('/', (req, res) => {
    let candidate = new Candidate();
    candidate.name = req.body.name;
    candidate.los = req.body.los;
    candidate.written_test = req.body.written_test;
    candidate.coding_test = req.body.coding_test;
    candidate.technical_interview = req.body.technical_interview;
    candidate.management_interview = req.body.management_interview;
    candidate.consultative_interview = req.body.consultative_interview;
    candidate.image = req.body.image;
    candidate.star = req.body.star;
    candidate.veto = req.body.veto;
    candidate.save((err, document) => {
        if (err) {
            res.status(400).send(err);
        } else {
            res.json(document);
        }
    })
})

candidateRouter.get('/', (_, res) => {
    Candidate.find((err, documents) => {
        if (err) {
            res.status(400).send(err);
        } else {
            res.json(documents);
        }
    })
})

candidateRouter.get('/:id', (req, res) => {
    Candidate.findById(req.params.id, (err, document) => {
        if (err) {
            res.status(400).send(err);
        } else {
            res.json(document);
        }
    })
})

candidateRouter.patch('/:id', (req, res) => {
    Candidate.findById(req.params.id, (err,document) => {
        if(err) {
           res.status(400).send(err); 
        }
        else {
            document.written_test = req.body.written_test;
            document.coding_test = req.body.coding_test;
            document.technical_interview = req.body.technical_interview;
            document.management_interview = req.body.management_interview;
            document.consultative_interview = req.body.consultative_interview;
            document.star = req.body.star;
            document.veto = req.body.veto;
            document.general_comment = req.body.general_comment;
            document.save();
            res.json(document);
        }

    })
})

// candidateRouter.put('/:candidate_id', (req, res) => {
//     Candidate.findById(req.params.candidate_id, (err, document) => {
//         if (err) {
//             res.status(400).send(err);
//         } else {
//             document.name = req.body.name;
//             document.written_test = req.body.written_test;
//             document.technical_test = req.body.technical_test;
//             document.technical_interview = req.body.technical_interview;
//             document.management_interview = req.body.management_interview;
//             document.speed_interview = req.body.speed_interview;

//             document.save((savedErr, savedDocument) => {
//                 if (savedErr) {
//                     res.stats(400).send(savedErr);
//                 } else {
//                     res.send(`candidate posted!\n${savedDocument}`);
//                 }
//             })
//         }
//     })
// })

candidateRouter.delete('/:candidate_id', (req, res) => {
    Candidate.deleteOne({
        _id: req.params.candidate_id
    }, (err, document) => {
        if (err) {
            res.status(400).send(err);
        } else {
            res.json(document);
        }
    })
})

module.exports = candidateRouter;