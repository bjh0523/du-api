const express = require('express');
const mongoose = require('mongoose');
const loginRouter = require('./api/routes/loginRouter');
const candidateRouter = require('./api/routes/candidateRouter');
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/api/login', loginRouter);
app.use('/api/candidate', candidateRouter);


app.use('/', (_, res) => {
    res.send('Welcome to the DU-Assessment API');
})

const port = process.env.PORT || 1985;

mongoose.connect('mongodb://localhost:27017/DU-Assessment', { useNewUrlParser: true })
mongoose.connection.on('connected', () => {
    console.log('Connected to the DU-Assessment DB');
})
mongoose.connection.on('error', () => {
    console.log(err);
})

app.listen(port, () => {
    console.log(`Listening on port ${port}.....`);
})